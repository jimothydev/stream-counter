What is it for?
====
Stream Counter is a simple program that allows streamers to easily keep track of anything and share that info with their viewers easily. This could be useful as a death, win, or attempt counter viewable live on stream.

How do I use it?
====
To download the program simply download the below .zip file.

https://gitlab.com/jimothydev/stream-counter/-/blob/master/StreamCounter.zip

Once it is downloaded extract the folder into your desired location (keeping the files together, of course). Once the folder is extracted you may simply run the Stream Counter.exe file. The increment value can be changed to your liking and the add and subtract functions can even be hotkeyed so you do not have to interrupt your stream to change the counter. To use the counter with OBS, create a new text source. In the properties menu of the text source, check "Read from file" and select the 'counter.txt' file in the 'Stream Counter_data' folder (the file will not appear until you run the program once).
