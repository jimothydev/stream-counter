﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using SimpleFileBrowser;
using System.IO;
using TMPro;
using System.Globalization;
using UnityEngine.UI;
using UnityRawInput;

public class App : MonoBehaviour
{

    string appPath;
    string counterFileName = "counter.txt";

    decimal incrementAmount = 1;
    decimal counter = 0;

    public TextMeshProUGUI incrementUpText;
    public TextMeshProUGUI incrementDownText;
    public TextMeshProUGUI addHotkeyText;
    public TextMeshProUGUI subtractHotkeyText;
    public TMP_InputField counterInput;
    public TMP_InputField incrementInput;
    public TMP_InputField templateInput;
    
    bool isSettingSubtractHotkey = false;
    bool isSettingAddHotkey = false;

    public RawKey subtractHotkey = RawKey.F23;
    public RawKey addHotkey = RawKey.F24;
    FileStream fs;

    public string template = "{cnt}";

    CultureInfo culture;

    void Start()
    {
        RawKeyInput.Start(true);
        RawKeyInput.OnKeyUp += HandleKeyUp;

        culture = new CultureInfo("en-US");

        //Get path of application
        appPath = Application.dataPath+@"/";

        if (!File.Exists(appPath + counterFileName))
        {
            fs = File.Create(appPath+counterFileName);
            fs.Close();
        }
        File.WriteAllText(appPath + counterFileName, "0");
    }

    public void UpdateTemplate()
    {
        if (templateInput.text.Contains("{cnt}"))
        {
            template = templateInput.text;
            UpdateCounterFile();
        }
    }

    public void BeginSubtractHotkey()
    {
        if (isSettingAddHotkey)
        {
            isSettingAddHotkey = false;
            addHotkeyText.text = "Hotkey";
        }
        isSettingSubtractHotkey = true;
        subtractHotkeyText.text = "...";
    }

    public void BeginAddHotkey()
    {
        if (isSettingSubtractHotkey)
        {
            isSettingSubtractHotkey = false;
            subtractHotkeyText.text = "Hotkey";
        }
        isSettingAddHotkey = true;
        addHotkeyText.text = "...";
    }

    void UpdateCounterFile()
    {
        string counterString = template.Replace("{cnt}", counter.ToString());
        File.WriteAllText(appPath + counterFileName, counterString);
    }

    public void AddIncrement()
    {
        counter+=incrementAmount;
        counterInput.text=counter.ToString();
        UpdateCounterFile();
    }

    public void SubtractIncrement()
    {
        counter-=incrementAmount;
        counterInput.text=counter.ToString();
        UpdateCounterFile();
    }

    public void UpdateCounter()
    {
        string tempText = counterInput.text;
        counter = Convert.ToDecimal(tempText, new CultureInfo("en-US"));
        UpdateCounterFile();
    }

    public void UpdateIncrement()
    {
        string tempText = incrementInput.text;
        incrementAmount = Convert.ToDecimal(tempText, new CultureInfo("en-US"));

        incrementUpText.text = "-" + tempText;
        incrementDownText.text = "+" + tempText;
    }

    private void HandleKeyUp (RawKey key) 
    {

        if (key == subtractHotkey)
        {
            SubtractIncrement();
        } else if (key == addHotkey)
        {
            AddIncrement();
        }

        if (isSettingAddHotkey)
        {
            if (key != subtractHotkey)
            {
                addHotkey = key;
                isSettingAddHotkey = false;
                addHotkeyText.text = Enum.GetName(typeof(RawKey), key);
            }
        } else if (isSettingSubtractHotkey)
        {
            if (key != addHotkey)
            {
                subtractHotkey = key;
                isSettingSubtractHotkey = false;
                subtractHotkeyText.text = Enum.GetName(typeof(RawKey), key);
            }  
        }
    }

    void OnApplicationQuit()
    {
        RawKeyInput.OnKeyUp -= HandleKeyUp;
        RawKeyInput.Stop();
    }
}
